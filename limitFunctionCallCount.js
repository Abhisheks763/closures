const limitFunctionCallCount=function (cb,n){
    let counter = 0
    return function func(i){
        counter++
        if (counter>n){return null}else{
            return cb(i);
        }

    }
}
module.exports = limitFunctionCallCount;

