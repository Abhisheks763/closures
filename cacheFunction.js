const cacheFunction = function (cb){
    let cache={}
    
    return function func(i){
        if (i in cache){return cache[i]}else{
            const a = cb(i)
            cache[i]=a
            console.log('updating cache',cache)
            return a
        }
    }
}



module.exports = cacheFunction;
